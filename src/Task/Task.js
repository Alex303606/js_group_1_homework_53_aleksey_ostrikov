import React from 'react';
import './Task.css';

const Task = props => {
	return (
		<div className="item">
			<span>{props.text}</span>
			<b onClick={props.check} className={props.status} aria-hidden="true"/>
			<button onClick={props.delete}>
				<i className="fa fa-trash-o" aria-hidden="true"/>
			</button>
		</div>
	)
};

export default Task;