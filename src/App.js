import React, {Component} from 'react';
import './App.css';
import ToDo from "./AddTaskForm/AddTaskForm";
import Task from "./Task/Task";

class App extends Component {
	
	state = {
		task: [
			{text: 'Buy milk.',status: false, id: 'b1'},
			{text: 'Make home work.',status: false, id: 'm1'},
			{text: 'Walk this dog.',status: false, id: 'w1'},
			{text: 'Wash a car.',status: false, id: 'w2'}
		],
		inputValue: 'Add new task'
	};
	
	addTask = () => {
		const taskText = this.state.inputValue;
		const tasks = this.state.task;
		let taskId = Date.now();
		let task = {text: taskText,id: taskId};
		tasks.push(task);
		const inputValue = 'Add new task';
		this.setState({tasks});
		this.setState({inputValue});
	};
	
	change = (event) => {
		let inputValue = [...this.state.inputValue];
		inputValue = event.target.value;
		this.setState({inputValue});
	};
	
	deleteTask = (id) => {
		const tasks = this.state.task;
		const index = tasks.findIndex(p => p.id === id);
		tasks.splice(index,1);
		this.setState({tasks});
	};
	
	checkTask = (id) => {
		const tasks = [...this.state.task];
		const index = tasks.findIndex(p => p.id === id);
		tasks[index].status = !tasks[index].status;
		this.setState({tasks});
	};
	
	render() {
		
		let tasks = null;
		
		tasks = (
			<div className="items">
				{this.state.task.map((task) => {
					let status = null;
					if(task.status) status = 'fa fa-check-square-o';
					else status = 'fa fa-square-o';
					return <Task
						key={task.id}
						text={task.text}
						status={status}
						check={() => this.checkTask(task.id)}
						delete={() => this.deleteTask(task.id)}
					/>
				})}
			</div>
		);
		
		return (
			<div className="main">
				<div className="container">
					<ToDo
						value={this.state.inputValue}
						add={() => this.addTask()}
						change={(event) => this.change(event)}
					/>
					{tasks}
				</div>
			</div>
		);
	}
}

export default App;
